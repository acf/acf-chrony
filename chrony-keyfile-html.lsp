<% local form, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% htmlviewfunctions.displaycommandresults({"enablekeyfile"}, session) %>

<%
	local header_level = htmlviewfunctions.displaysectionstart(form.value.status, page_info)
	htmlviewfunctions.displayitem(form.value.status)
	if form.value.status.errtxt then
		htmlviewfunctions.displayitem(cfe({type="form", value={redir=cfe({ type="hidden", value=page_info.orig_action })}, label="", option="Enable", action="enablekeyfile" }), page_info, 0)
	end
	htmlviewfunctions.displaysectionend(header_level)
%>

<%
local pattern = string.gsub(page_info.prefix..page_info.controller, "[%(%)%.%%%+%-%*%?%[%]%^%$]", "%%%1")
local func = haserl.loadfile(page_info.viewfile:gsub(pattern..".*$", "/") .. "filedetails-html.lsp")
func(form, viewlibrary, page_info, session)
%>
