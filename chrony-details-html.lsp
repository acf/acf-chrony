<% local data, viewlibrary = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% viewlibrary.dispatch_component("status") %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% local header_level2 = htmlviewfunctions.incrementheader(header_level) %>
<% htmlviewfunctions.displayitem(data.value.time) %>
<% htmlviewfunctions.displayitem(data.value.tracking) %>
<% htmlviewfunctions.displaysectionstart(data.value.sources, page_info, header_level2) %>
<pre><%= html.html_escape(data.value.sources.value) %></pre>
<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionstart(data.value.sourcestats, page_info, header_level2) %>
<pre><%= html.html_escape(data.value.sourcestats.value) %></pre>
<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
