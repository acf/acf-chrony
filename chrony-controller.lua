local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.details(self)
	return self.model.getdetails()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.config(self)
	return self.handle_form(self, self.model.get_config, self.model.update_config, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.keyfile(self)
	return self.handle_form(self, self.model.get_keyfiledetails, self.model.update_keyfiledetails, self.clientdata, "Save", "Edit Key File", "Key File Saved")
end

function mymodule.enablekeyfile(self)
	return self.handle_form(self, self.model.get_enable_keyfile, self.model.enable_keyfile, self.clientdata, "Enable", "Enabled Key File", "Enabled key file")
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filedetails, self.clientdata, "Save", "Edit Config File", "Configuration Set")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
